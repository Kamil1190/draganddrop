import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { IDraggable } from './models/draggable';
import { DraggableService } from './partials/draggable.service';

@Component({
    moduleId: module.id,
    selector: 'app',
    template: `
        <div>
            <h1>Angular & InteractJS: Drag and drop</h1>
            <draggable-container [items]='draggableItems'></draggable-container>
            <div>
                <droppable id='dropzone'></droppable>
            </div>
        </div>
    `,
    styleUrls: ['app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit{

    draggableItems: IDraggable[] = [
        {
            selectorId: "drag1",
            id: 1,
            name: 'drag1',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag2",
            id: 2,
            name: 'drag2',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag3",
            id: 3,
            name: 'drag3',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag4",
            id: 4,
            name: 'drag4',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag5",
            id: 5,
            name: 'drag5',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag6",
            id: 6,
            name: 'drag6',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag7",
            id: 7,
            name: 'drag7',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag8",
            id: 8,
            name: 'drag8',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag9",
            id: 9,
            name: 'drag9',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag10",
            id: 10,
            name: 'drag10',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag11",
            id: 11,
            name: 'drag11',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }, 
        {
            selectorId: "drag12",
            id: 12,
            name: 'drag12',
            parent: 'default',
            xPosition: 0,
            yPosition: 0
        }
    ];

    constructor(private draggableService: DraggableService) {}

    ngOnInit(): void {

    }



}
