import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { DraggableComponent } from './partials/draggable/draggable.component';
import { DroppableContainerComponent } from './partials/droppable/droppable.container.component';
import { DraggableContainerComponent } from './partials/draggable_container/draggable.container.component';
//SERVICES
import { DraggableService } from './partials/draggable.service';

@NgModule({
  imports: [ BrowserModule ],
  declarations: [ 
    AppComponent,
    DraggableComponent,
    DroppableContainerComponent,
    DraggableContainerComponent
  ],
  providers: [
    DraggableService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
