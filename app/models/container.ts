import { IDraggable } from './draggable';

export interface IContainer {
    name: string;
    items: IDraggable[];
}

export class Container implements IContainer{
    
    constructor(public name: string, public items: IDraggable[]){}

}