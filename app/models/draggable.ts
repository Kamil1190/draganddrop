export interface IDraggable{
    selectorId: string;
    id: number;
    xPosition: number;
    yPosition: number;
    name: string;
    parent: string;
}