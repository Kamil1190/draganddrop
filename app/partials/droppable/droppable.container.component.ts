import { Component, ViewEncapsulation } from '@angular/core';
import { IDraggable } from '../../models/draggable';
import { DraggableService } from '../draggable.service';

@Component({
    moduleId: module.id,
    selector: 'droppable',
    styleUrls: ['droppable.container.component.css'],
    template: `
        <h1 class="center">{{draggableService.getContainer(nameDropzone).length}}</h1>
        <dragging *ngFor='let item of draggableService.getContainer(nameDropzone)' 
                    id='{{item.selectorId}}' 
                    attr.parent='{{item.parent}}'
                    [element]='item' 
                    (notify)='onDraggingChange($event)'
                    class="well well-sm"
                    >
        </dragging>
    `,
    encapsulation: ViewEncapsulation.None
})
export class DroppableContainerComponent{

    nameDropzone: string = "dropzone";

    constructor(private draggableService: DraggableService) {
        // Create and run new dropzone
        this.draggableService.addDropzone(this.nameDropzone);
    }


    onDraggingChange(element: IDraggable): void {
        this.draggableService.logDraggable(element, "Emitt from clone");
    }
}