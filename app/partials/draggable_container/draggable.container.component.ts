import { Component, Input, ViewEncapsulation, OnInit } from '@angular/core'
import { IDraggable } from '../../models/draggable';
import { IContainer, Container } from '../../models/container';
import { DraggableService } from '../draggable.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
    moduleId: module.id,
    selector: 'draggable-container',
    template: `
    <div id="draggableService.defaultHelperName">
        <dragging *ngIf='draggableService.defaultHelper' 
                id='{{draggableService.defaultHelper.selectorId}}'
                attr.parent='{{draggableService.defaultHelper.parent}}' 
                [element]='draggableService.defaultHelper' 
                (notify)='onDraggingChange($event)'
                class="well well-sm"
                >
        </dragging>
    </div>
    <div class="scroll-box">
        <div id='{{nameDropzone}}' class="draggable-container">
            <dragging *ngFor='let item of draggableService.getContainer(nameDropzone)' 
                        id='{{item.selectorId}}'
                        attr.parent='{{item.parent}}'
                        [element]='item' 
                        (notify)='onDraggingChange($event)'
                        class="well well-sm"
                        >
            </dragging>
        </div>
    </div>
    `,
    styleUrls: ['draggable.container.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class DraggableContainerComponent implements OnInit {

    @Input() items: IDraggable[];
    private nameDropzone: string = "default";

    constructor(private draggableService: DraggableService) {}

    ngOnInit(): void {
        // When input array come in
        Observable.of(this.items).subscribe((items) => {
            // Create new container with this array and own name
            var container = new Container(this.nameDropzone, items);
            // set and run draggable main container
            this.draggableService.setMainContainer(container);
        });
    }

    onDraggingChange(element: IDraggable): void {
        this.draggableService.logDraggable(element, "Emitt from orginal");
    }
}
