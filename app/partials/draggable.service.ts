import { Injectable, NgZone } from '@angular/core';
import { IDraggable } from '../models/draggable';
import { IContainer, Container } from '../models/container';

interface IDraggableService {

    defaultHelper: IDraggable;
    defaultHelperName: string;
    containers: IContainer[];

    setMainContainer(container: IContainer): void;
    addDropzone(name: string): void;
    logDraggable(item: IDraggable): void;
}

@Injectable()
export class DraggableService implements IDraggableService {

    public defaultHelper: IDraggable;
    public defaultHelperName: string = 'defaultHelper';
    public defaultContainerName: string;
    public containers: IContainer[] = [];

    private actualEvent: any;


    constructor(private zone: NgZone) { }


    getContainer(name: string): IDraggable[] {
        return this.containers.filter((item) => item.name == name)[0].items;
    }

    setMainContainer(container: IContainer): void {
        this.containers.push(container);
        this.defaultContainerName = container.name;
        this.enableDragging();
        this.enableDropBack();
    }

    addDropzone(containerName: string): void {
        this.containers.push(new Container(containerName, []));
        this.createInteractDropzone(containerName);
        console.log('DROPZONE ' + containerName + ' CREATED.');
    }

    logDraggable(model: IDraggable, message: string = ""): void {
        console.log(message + ": " + model.selectorId + ", " + model.id + ", " + model.name + ", " + model.xPosition + ", " + model.yPosition);
    }

    // Enable dragging
    private enableDragging(): void {
        interact('dragging').draggable({
            inertia: false,
            manualStart: true,
            restrict: {
                restriction: "parent",
                endOnly: true,
                elementRect: { left: 0, right: 1, top: 0, bottom: 1 }
            },
            onstart: (event: Interact.InteractEvent) => {
                
            },
            onmove: (event: Interact.InteractEvent) => {
                var view = event.target,
                    // keep the dragged position in the data-x/data-y attributes
                    x = Math.floor((parseFloat(view.getAttribute('data-x')) || 0) + event.dx),
                    y = Math.floor((parseFloat(view.getAttribute('data-y')) || 0) + event.dy);

                // translate the element
                view.style.webkitTransform =
                    view.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';

                // update the posiion attributes
                view.setAttribute('data-x', x);
                view.setAttribute('data-y', y);

                var model = this.getModel(view);
                if(model){
                    this.updatePosition(model, x, y);
                }
            }
        }).on('move', (event: any) => {
            var interaction = event.interaction;
            var view = event.currentTarget as HTMLElement;

            //if parent view is default container, interact clone
            if (interaction.pointerIsDown && !interaction.interacting()) {

                if(view.getAttribute('parent') == this.defaultContainerName){
                    // 1. Get model
                    var model = this.getModel(view);

                    // 2. Save actual superposition element
                    var viewRect = view.getBoundingClientRect();

                    // 2. Move to default helper
                    var newView = this.moveViewToDefaultHelper(model);

                    // 3. Set CSS params and superposition
                    newView.classList.add('clone');
                    newView.style.left = viewRect.left + 'px';
                    newView.style.top = viewRect.top + 'px';

                    // 4. Start interacting with new DOM element
                    //this.changeInteraction(event, newView);
                    interaction.start({ name: 'drag' },
                        event.interactable,
                        newView);
                } else {
                    interaction.start({ name: 'drag' },
                        event.interactable,
                        view);
                }

            }
            this.actualEvent = event;
        });
        console.log("DRAGGABLE ENABLED");
    }

    private enableDropBack(): void {

        interact('#' + this.defaultContainerName).dropzone({
            accept: 'dragging',
            ondragenter: (event: any) => {

                var view = event.relatedTarget as HTMLElement;

                if (view.getAttribute('parent') != this.defaultContainerName) {

                    var containerView = event.target as HTMLElement;
                    var scrollBoxView = containerView.parentElement;
                    var interaction = event.interaction;

                    // 1. Set scrollbox view
                    scrollBoxView.style.borderColor = "green";

                    // 2. Get Model
                    var model = this.getModel(view);
                    var viewRect = view.getBoundingClientRect();

                    // 3. Move element from list to defaultHelper
                    var newView = this.moveViewToDefaultHelper(model);

                    // 4. Active draggable border
                    newView.style.top = viewRect.top + 'px';
                    newView.style.left = viewRect.left + 'px';
                    newView.classList.add('drop-active');
                    newView.classList.add('clone');

                    // 5. Start interact element
                    //this.changeInteraction(event, newView);
                    interaction.start({ name: 'drag' },
                        event.interactable,
                        newView);
                }
            },
            ondragleave: (event: any) => {
                var container = event.target as HTMLElement;
                var scrollBox = container.parentElement;
                scrollBox.style.borderColor = "black";
            },
            ondrop: (event: any) => {
                var container = event.target as HTMLElement;
                var scrollBox = container.parentElement;
                scrollBox.style.borderColor = "black";

                var view = event.relatedTarget as HTMLElement;

                // 1. Get model
                var model = this.getModel(view);

                // 2. Move model from defaultHelper to default
                this.moveTo(model, this.defaultContainerName);
            }
        });
        console.log("ENABLE DROP BACK");
    }

    private createInteractDropzone(containerName: string): void {

        interact('#' + containerName).dropzone({
            accept: 'dragging',
            ondragenter: (event: any) => {

                var view = event.relatedTarget as HTMLElement;

                if (view.getAttribute('parent') != containerName) {

                    var dropzoneView = event.targer as HTMLElement;
                    var interaction = event.interaction;

                    console.log(interaction);

                    // 1. Get model
                    var model = this.getModel(view);
                    // 2. Save actual superposition element
                    var viewRect = view.getBoundingClientRect();

                    // 3. Move model to this container and get new view
                    var newView = this.moveViewTo(model, containerName);
                    //interaction.target = newView;

                    // 4. Active draggable border
                    newView.style.top = viewRect.top + 'px';
                    newView.style.left = viewRect.left + 'px';
                    newView.classList.add('drop-active');
                    newView.classList.add('clone');
                    // 5. Start interact with new view

                    //interaction._updateEventTargets(newView, view);
                    interaction.start({ name: 'drag' }, this.actualEvent, newView);
                    //this.changeInteraction(event, newView);
                }

            },
            ondragleave: (event: any) => {
                var view = event.relatedTarget as HTMLElement;
                view.classList.remove('drop-active');
            },
            ondrop: (event: any) => {
                var item = event.relatedTarget as HTMLElement;
                item.classList.remove('drop-active');
                item.classList.add('droped');
            }

        });
    }

    //remove from old container and push to new container
    private moveTo(model: IDraggable, parent: string): IDraggable {

        // 1. remove from list
        this.removeView(model);
        // 2. change parent name
        model.parent = parent;
        // 3. push to new container
        this.getContainer(parent).push(model);
        // 4. return instance from container
        this.zone.run(() => { });
        return this.getContainer(parent)[this.getContainer(parent).indexOf(model)];

    }

    private moveViewTo(model: IDraggable, containerName: string): HTMLElement {
        this.moveTo(model, containerName);
        return document.getElementById(model.selectorId) as HTMLElement;
    }

    private moveViewToDefaultHelper(model: IDraggable): HTMLElement {

        // 1. remove from list
        this.removeView(model);
        // 2. change parent name
        model.parent = this.defaultContainerName;
        // 3. push to new container
        this.defaultHelper = model;
        // 4. return instance from container
        this.zone.run(() => { });
        return document.getElementById(model.selectorId) as HTMLElement;
    }

    private changeInteraction(event: any, view: HTMLElement): any {

        this.actualEvent.interaction.start({ name: 'drag' },
            this.actualEvent.interactable,
            view);

        return event;
    }

    private getModelById(list: IDraggable[], id: string): IDraggable {
        return list.filter(item => item.selectorId == id)[0];
    }

    private getModel(view: HTMLElement): IDraggable {
        var parent = view.getAttribute('parent') as string;
        var idView = view.getAttribute('id') as string;

        if (this.defaultHelper != null) {
            return this.defaultHelper;
        } else
            return this.getModelById(this.getContainer(parent), idView);
    }

    private updatePosition(model: IDraggable, x: number, y: number): void {
        this.zone.run(() => {
            model.xPosition = x;
            model.yPosition = y;
        });
    }

    private removeView(model: IDraggable) {

        if (model.parent == this.defaultContainerName && this.getContainer(this.defaultContainerName).indexOf(model) == -1) {
            this.defaultHelper = null;
        } else {
            this.remove(this.getContainer(model.parent), model);
        }
    }

    private remove(list: IDraggable[], item: IDraggable): void {
        let index: number = list.indexOf(item);
        if (index !== -1) {
            list.splice(index, 1);
        }
    }

}