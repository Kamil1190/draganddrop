//  import * as _ from 'interact';
import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core'
import { IDraggable } from '../../models/draggable';
import { DraggableService } from '../draggable.service';

@Component({
    moduleId: module.id,
    selector: 'dragging',
    styleUrls: ['draggable.component.css'],
    template: `
    <div>
        <p style="float: left" [innerHTML]='element.name'></p>
        <button style="float: right" (click)='showAlert()'>open</button>
        <h3 style="clear: both" [innerHTML]='element.xPosition+", "+element.yPosition'></h3>
    </div>
     `,
     encapsulation: ViewEncapsulation.None
})
export class DraggableComponent{

    @Input() element: IDraggable;
    @Output() notify: EventEmitter<IDraggable> = new EventEmitter<IDraggable>();

    constructor(private draggableService: DraggableService) { }


    showAlert(): void{
        alert(this.element.name+" ("+this.element.xPosition+", "+this.element.yPosition+")");
        this.notify.emit(this.element);
    }
}